const fs = require("fs");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const fullSiteStr = fs.readFileSync(`generated.public/index.html`,"utf8");
const dom = new JSDOM(fullSiteStr,"utf8");
dom.window.document.querySelectorAll('[id^="session_"]').forEach((n,k,all)=>{

  let nav = '';
  if(k>0) {
    const prev = JSON.parse(all[k-1].querySelector('script').innerHTML)['@graph'][0];
    nav += `<a class="nav previous" href="./${all[k-1].id}.html" title="Session précédente : ${prev.name}"><img src="img/picto/previous.svg" alt="&lt;-"/></a>`;
  }
  if(k<all.length-1) {
    const next = JSON.parse(all[k+1].querySelector('script').innerHTML)['@graph'][0];
    nav += `<a class="nav next" href="./${all[k+1].id}.html" title="Session suivante : ${next.name}"><img src="img/picto/next.svg" alt="-&gt;"/></a>`;
  }
  const domToInsert = (new JSDOM(nav,"utf8")).window.document.body.children;
  tab(domToInsert).forEach(part=>n.insertBefore(part,n.firstChild));
});
fs.writeFileSync(`generated.public/full_site.html`,dom.serialize(),"utf8");

const mainSections = dom.window.document.querySelectorAll("body>*");
tab(mainSections).filter(n=>n.id!=="espace-accueil" && n.tagName.toLowerCase()!=="header").forEach(n=>n.remove());
dom.window.document.body.appendChild(
	(new JSDOM(`<div>
<noscript>
	Sans JavaScript ? &gt;&gt; <a href="./full_site.html">Site Complet</a>
</noscript>
<script>
async function loadFullPage(){
	const page = await(await fetch('./full_site.html')).text();
	page.replace(new RegExp("<body>([^]+)</body>"),(fullBody,innerBody)=>document.body.innerHTML=innerBody);
	window.runJS();
}
async function swInit(){
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service_worker.js').then(
      (reg)=>{
       console.log('Service worker accepté', reg);
       navigator.serviceWorker.ready.then(loadFullPage);
      }, (err)=>{
        console.log('Service worker refusé', err);
        loadFullPage()
      });
  } else {
    console.log('Service workers are not supported.');
    loadFullPage();
  }
}
swInit();
</script></div>
`)).window.document.body.querySelector('div'));

let pageStr = dom.serialize();
const pageParts = pageStr.split(' id="');
const part0 = pageParts.shift();
const part1 = pageParts.shift();
let otherParts = pageParts.join(' id="');
otherParts = otherParts.replace(/<script type="application\/ld\+json">((?!<\/script>)[^])*<\/script>/g,'');
if(otherParts) pageStr=`${part0} id="${part1} id="${otherParts}`;
else pageStr=`${part0} id="${part1}`;


fs.writeFileSync(`generated.public/index.html`,pageStr,"utf8");

function tab(obj){
  return Object.keys(obj).map(k=>obj[k])
}
