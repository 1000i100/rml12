# Audit citoyen

A l'instar des logiciels libres, pour qu'une monnaie soit libre, son code monétaire doit être public
et chacun doit pouvoir contrôler qu'il est bien respecté.

C'est une des sources de confiance dans le fonctionnement et la pérennité de la monnaie.

Ces règles publiques sont également un outil précieux pour se questionner sur la chose monétaire,
se l'approprier, et innover en proposant des monnaies alternatives,
qui répondront ou non à un besoin sociétal, comme leur adoption ou non en témoignera.

## Logiciel Libre

Le moyen d'atteindre cet objectif qu'a choisi la communauté de la June/Ǧ1 avec Duniter est de s'appuyer
sur des logiciels libres et décentralisés.

S'appuyer sur des logiciels libres (Duniter en premier lieu) permet à qui prend le temps de s'y former,
de vérifier que le programme fait bien ce qu'il annonce :
faire fonctionner la monnaie tel qu'annoncé.

## Architecture décentralisée

Le fonctionnement décentralisé évite la prise de pouvoir par une entité centrale,
et permet à chacun de gérer un maillon du système, et ainsi d'en assurer la cohérence.

En effet, toute tentative de fraude monétaire d'un des maillons serait détectée automatiquement et écartée du système
par les autres maillons.
Ainsi, à moins d'avoir pour complice la majorité des maillons du système monétaire, pas de fraude.

Et si la majorité est complice, ce n'est plus une fraude mais un référendum réussi,
car chaque citoyen peut gérer un maillon du système monétaire pour exprimer sa voix et éviter les fraudes.
