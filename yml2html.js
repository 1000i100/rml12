const yaml = require('js-yaml');
const fs = require('fs');
const {addSeconds, addMonths,format} = require('date-fns');
const dateFormat = format;
const fr = require('date-fns/locale/fr');
const md = require('marked');
md.setOptions({headerIds: false, xhtml: true});

const data = yaml.safeLoad(fs.readFileSync('generated.programme.yml', 'utf8'));
const conf = yaml.safeLoad(fs.readFileSync('event.config.yml', 'utf8'));

function timeUnit(str){
	const res = {h:0,m:0,s:0};
	if(str.indexOf('h')>0) {
		res.h = parseInt(str.split('h')[0].trim());
		res.m = parseInt(str.split('h')[1].split('m')[0].trim())||0;
		str = str.split('h')[1];
	}
	if(str.indexOf('m')>0) {
		res.m = parseInt(str.split('m')[0].trim());
		res.s = parseFloat(str.split('m')[1].split('s')[0].trim())||0;
	}
	return res;
}
function hms2s(hms){
	return hms.h*3600+hms.m*60+hms.s;
}
function duration2s (duration){
	if(typeof duration === "string") return hms2s(timeUnit(duration));
	if(typeof duration === "object"){
		let res = 0;
		for (let i in duration) {
			res += hms2s(timeUnit(duration[i]));
		}
		return res;
	}
	return duration;
}
function formatDuration(seconds){
	const midNight = new Date('2000-01-01T00:00:00.000Z');
	const atDuration = addSeconds(midNight,seconds);
	const hours = Math.floor(seconds/3600);
	const minutes = Math.floor((seconds-hours*3600)/60);
	if(hours>0) return `${hours}h${minutes?dateFormat(atDuration, 'mm'):''}`;
	return `${minutes} minutes`;
}
function date2ymd(date) {
	return {y:date.getFullYear(),
		month:date.getMonth()+1,
		d: date.getDate()
	};
}
function computeStartTime(date,startTime){
	const {y, month, d} = date2ymd(new Date(date)),
		{h,m,s} = timeUnit(startTime);
	return new Date(y, month, d, h, m,s)
}
function parentOrSelf(anything){
	if(!anything.parent) return anything;
	return anything.parent;
}
function placeCrumb(place,html='') {
	if(!place.parent) {
		html = `<a href="#place_${place.uid}">${place.title}</a> &gt; ${html}`;
		return html.substring(0,html.length-6);
	}
	html = `<span${place.description?` title="${place.description}"`:''}>${place.title}</span> &gt; ${html}`;
	return placeCrumb(place.parent,html);
}
function buildHtmlRoomSymbol(streamNumber,streamTotal, streamTitle){
	//const roomList = '█▌▐▐';//⌂⏏⛊🠷🅐🅑🅒🅓🅔🅕🅖🅗🅘🅙🅠🅡🅢🅣🅤🅥🅦🅧🅨🅩𝆺𝅥❚⛊▉█▌┃⏏🜚🝯🟇🟆🠷🖈📌📍•';
	const roomClass = `roomRef room_${streamNumber} room_${streamNumber}_in_${streamTotal}`;
	//const roomNode = dayContent.streams.length>1?`<span class="${roomClass}" title="${stream.room.title}">${roomList.substr(currentStreamNumber,1)}</span> `:'';
	const roomNode = `<span class="${roomClass}" title="${streamTitle}">⬤</span>&nbsp;`;
	return roomNode;
}
function genreNombre(listPersonnes,zero,masculin,feminin,masculinPluriel,femininPluriel,mixPluriel) {
	switch (listPersonnes.length) {
		case 0: return zero;
		case 1: return listPersonnes[0].genre.indexOf('f')!== -1 ? feminin : masculin;
		default:
			return listPersonnes.reduce((acc,curr)=>{
				switch (acc) {
					case zero:
						if (curr.genre.indexOf('f') !== -1) return femininPluriel;
						else return masculinPluriel;
					case femininPluriel:
						if (curr.genre.indexOf('f') !== -1) return femininPluriel;
						else return mixPluriel;
					case masculinPluriel:
						if (curr.genre.indexOf('f') !== -1) return mixPluriel;
						else return masculinPluriel;
					case mixPluriel:
					default:
						return mixPluriel;
				}
			},zero);
	}
}
function semanticName(name) {
	let pseudo;
	let firstname;
	let lastname;
	const nameParts = name.split(' ');
	if(nameParts[0][0]==='[') pseudo = nameParts.shift().slice(1,-1);
	firstname = nameParts.shift();
	lastname = nameParts.join(' ');
	const res = {};
	if(pseudo) res["alternateName"]=pseudo;
	if(firstname) res["givenName"]=firstname;
	if(lastname) res["familyName"]=lastname;
	return res;
}
function computeBannerFileName(speakers) {
	if(!speakers.length) return conf.og.default_image;
	let name = `session__${speakers.filter(s=>!!s.photo).map(speaker=>speaker.photo.substring(speaker.photo.lastIndexOf('/')+1,speaker.photo.lastIndexOf('.'))).join('__')}.jpg`;
	if(name === 'session__.jpg') return conf.og.default_image;
	if(name.length>250) name = `${speakers.filter(s=>!!s.photo).map(speaker=>speaker.photo.substring(speaker.photo.lastIndexOf('/')+1,speaker.photo.lastIndexOf('.')).split('_')[0]).join('_')}.jpg`;
	if(speakers.length>10) return conf.og.default_image;//TODO: à corriger en même temps que le générateur de bandeau
	return name;
}
function buildSession(session, startTime, endTime) {
	const stream = session.parent;
	const dayContent = stream.parent;
	const dateFR = formatDateFR(dayContent.date);

	let tags = session.tags ? session.tags.split(',').map(str => str.trim()) : [];
	if (session.type) tags.unshift(session.type);
	//if(session.ics !== false)
	const startMinutes = startTime.getMinutes() ? dateFormat(startTime, 'mm') : '';
	const formatedStartTime = dateFormat(startTime, 'H') + 'h' + startMinutes;
	const endMinutes = endTime.getMinutes() ? dateFormat(endTime, 'mm') : '';
	const formatedEndTime = dateFormat(endTime, 'H') + 'h' + endMinutes;
	const speakers = session.who ? (Array.isArray(session.who) ? session.who : [session.who]) : [];
	const intervenantEs = genreNombre(speakers, '', 'Intervenant', 'Intervenante', 'Intervenants', 'Intervenantes', 'IntervenantEs');
	const roomNumber = dayContent.streams.map((s,i)=>s===stream?i+1:'').join('')
	const roomClass = `room_${roomNumber} room_${roomNumber}_in_${dayContent.streams.length}`;
	const speakersClasses = speakers.map(speaker=>`speaker_${speaker.uid}`).join(' ');
	const jsonLD = {
		"@context": "http://schema.org",
		"@graph": [
			{
				"@type": "Event",
				"name": session.title,
				"startDate": dateFormat(startTime),
				"endDate": dateFormat(endTime),
				"isAccessibleForFree": !session.price,
				"image": `${conf.canonical}${conf.og.images_path}${computeBannerFileName(speakers)}`,
				"description": ((session.disclaimer || '')+" \n" + (session.goal || '')+" \n" + (session.description || 'Plus de détails prochainement.')).trim(),
				"location": {
					"@type": "Place",
					"name": `${stream.room.parent.title} > ${stream.room.title}`,
					"isAccessibleForFree": true,
					"url": stream.room.parent.web,
					"sameAs": `./place_${stream.room.parent.uid}.html`,
					"address": address2json(stream.room.parent.address),
					"geo": {
						"@type": "GeoCoordinates",
						"latitude": stream.room.parent.gps.lat,
						"longitude": stream.room.parent.gps.lon
					},
					"description": stream.room.parent.description + "\n" + stream.room.description
				},
        "offers":[
          {
            "@type": "Offer",
            "url": `${conf.canonical}session_${session.uid}.html`,
            "price": session.price || 0,
            "priceCurrency":"Ǧ1",
            "availability": "http://schema.org/InStock",
            "validFrom": dateFormat(addMonths(new Date(conf.date.start),-6),'YYYY-MM-DD')
          }
        ],
				"performer":speakers.map( speaker => {
					return {
						...{
							"@type": "Person",
							"name": speaker.title,
							//"sameAs" : "http://en.wikipedia.org/wiki/Typhoon_(American_band)",
							"image": speaker.photo,
							"gender": speaker.genre.indexOf('f') !== -1 ? 'Female' : 'Male',
							"url": speaker.web?speaker.web.replace(/^.*\(http(.*)\)/,(all,url)=>'http'+url):'',
							"email": speaker.publicEmail||'',
							"description":speaker.description
						},
						...semanticName(speaker.title)
					};})
			}]
		};
	if(stream.room.parent.photo) jsonLD['@graph'][0].location.photos = stream.room.parent.photo.map(urlPhoto =>{return {"@type":"ImageObject", "contentUrl":`${urlPhoto}`};});
	if(jsonLD['@graph'][0].performer.length===0) jsonLD['@graph'][0].performer.push({
    "@type": "Organization",
    "name": conf.og.default_author,
  });

	let record='';
  if(session.recorded===true)  record = `<a class="record infoBoxParent" data-start-live="${dateFormat(startTime)}" data-end-live="${dateFormat(endTime)}" href="https://www.youtube.com/channel/UCTFiEd7f38oSl-Bo5HqJzyg"><img src="img/picto/rec/recorded.svg" alt="📹"/><span class="infoBox">Sera diffusé en live puis disponible sur Peertube.</span></a>`;
  if(session.recorded===false) record = `<a class="record infoBoxParent"><img src="img/picto/rec/not_recorded.svg" alt="not recorded"/><span class="infoBox">Non filmé.</span></a>`;
  if(typeof session.recorded==='string') session.recorded = [session.recorded];
  if(typeof session.recorded==='object') {
    record = `<a class="record infoBoxParent" href="${session.recorded[0]}"><img src="img/picto/rec/recorded.svg" alt="📹"/><span class="infoBox">Disponible sur peertube.</span></a>`;
  }

  return `
<div class="session"><a href="#session_${session.uid}" class="subPageLink ${roomClass} ${speakersClasses}"><span class="startTime">${formatedStartTime}</span> - ${session.title}</a></div>
<section id="session_${session.uid}" class="subPage">
	<script type="application/ld+json">${JSON.stringify(jsonLD)}</script>
		<article class="eventArea">
		  <div class="titleBar">
		  ${record}
		  </div>
		  <h3>${session.title}</h3>
	    <div class="place">${placeCrumb(stream.room)}</div>
	    <div class="time desc">${dateFR} de <strong>${formatedStartTime} à ${formatedEndTime}</strong> (durée ${formatDuration(duration2s(session.duration || 0))})</div>
		${session.disclaimer ? `<div class="disclaimer desc">${md(session.disclaimer || '')}</div>` : ''}
		${typeof session.recorded==='object' ? `
      <div class="video">
        <a class="videoArea" href="${session.recorded[0]}"><img src="img/picto/rec/peertube.png" alt="Disponible sur peertube."/></a>
        <div class="mirrors">Vidéo également disponible sur : ${session.recorded.map(v=>`<a href="${v}">${url2domain(v)}</a>`).join(', ')}</div>
      </div>` : ''}
	    <h4>Publics :</h4>
	    <div>${md(session.target || 'Destiné à être accessible au plus grand nombre, sans connaissances spécifiques. Jeunes et adultes.')}</div>
		${session.goal ? `<h4>Objectif :</h4>\n<div class="desc">${md(session.goal || '')}</div>` : ''}
	    ${session.description ? `<h4>Résumé :</h4>\n<div class="desc">${md(session.description || '')}</div>` : ''}
	</article>
    <aside class="speakersArea">
    	${intervenantEs ? `<h3>${intervenantEs} :</h3>` : ''}
    	${speakers.map(
		(speaker) => `
<div class="speaker">
	${speaker.photo ? `<img class="avatar" src="${speaker.photo}" alt="${speaker.title}"/>` : ''}
	<h4 class="desc">${speaker.title}</h4>
	${speaker.web ? (md(speaker.web)).replace(new RegExp("<p>(.*)</p>"),(o,c)=>c) : ''}
	${speaker.publicEmail ? `<a href="mailto:${speaker.publicEmail}">${speaker.publicEmail}</a>` : ''}
	<div class="description">
		${md(speaker.description)}
	</div>
</div>
`).join("\n")}
	</aside>
</section>
`;
}
function url2domain(v) {
  return v.split('/')[2];
}
function computeStream(stream) {
	const sessionsStream = [];
	const dayContent = stream.parent;
	let startTime = computeStartTime(dayContent.date, dayContent.startTime);
	for (let session of stream.sessions) {
		session.parent = stream;
		if (session.setTime) {
			startTime = computeStartTime(dayContent.date, session.setTime);
			continue;
		}
		if (session.pause) {
			startTime = addSeconds(startTime, duration2s(session.pause || 0));
			continue;
		}
		const endTime = addSeconds(startTime, duration2s(session.duration || 0));
		session.start = startTime;
		session.end = endTime;
		session.html = buildSession(session, startTime, endTime);
		sessionsStream.push(session);
		startTime = addSeconds(endTime, duration2s(dayContent.parent.config.afterEachPause || 0));
	}
	return sessionsStream;
}
function upperFirstChr(str) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}
function formatDateFR(strDate) {
	return upperFirstChr(`${dateFormat(new Date(strDate), 'dddd D/M/YYYY',{ locale: fr }, )}`);
}
function extractSpeakers(str){
	const extract = {};
	str.replace(/speaker_([^ "]+)/g,(full,speaker)=>extract[speaker]=speaker);
	return Object.keys(extract);
}
function buildDay(allStreamsSessions, dayContent, dayTimeline, places, placesSubPages) {
	const extractStartTime = (str) => duration2s(str.match(/<span class="startTime">([0-9hm]+)</)[1]);
	allStreamsSessions.sort((a, b) => extractStartTime(a) - extractStartTime(b));

	const todaySpeakers = extractSpeakers(allStreamsSessions.join(''));
  const jsonSessions = allStreamsSessions.map(extractJsonLD).map(s=>s['@graph'][0]);
  let jsonSpeakers = jsonSessions.map(s=>s.performer).reduce((acc,cur)=>{
    cur.forEach(c=>acc[c.name]=c);
    return acc;
  },{});
  jsonSpeakers = Object.keys(jsonSpeakers).map(k=>jsonSpeakers[k]);

	const dateFR = formatDateFR(dayContent.date);
	const id_date = dateFR.replace(/[^a-z0-9]/ig, '_').replace(/_+/g, '_');
	const jsonLD = {
		"@context": "http://schema.org",
		"@graph": [
			{
				"@type": "Event",
				"name":  `Journée : ${dayContent.title}`,
				"startDate": jsonSessions[0].startDate,
				"endDate": jsonSessions[jsonSessions.length-1].endDate,
				"isAccessibleForFree": true,
				"inLanguage": "French",
				"image": `${conf.canonical}${conf.og.images_path}${computeBannerFileName(todaySpeakers)}`,
				"description": jsonSessions.filter(s=>s.name.indexOf("déjeuner")=== -1).map(s=>s.name).join(' ~ '),
				"location": jsonSessions[0].location,
				"performer": jsonSpeakers,
        "offers":[
          {
            "@type": "Offer",
            "url": `${conf.canonical}${id_date}.html`,
            "price": 0,
            "priceCurrency":"Ǧ1",
            "availability": "http://schema.org/InStock",
            "validFrom": dateFormat(addMonths(new Date(conf.date.start),-6),'YYYY-MM-DD')
          }
        ],
        "subEvent": jsonSessions
			}
		]
	};
	return `
            <li id="${id_date}" class="day ${todaySpeakers.map((speaker)=>`speaker_${speaker}`).join(' ')}">
			    <script type="application/ld+json">${JSON.stringify(jsonLD)}</script>
    			<input type="hidden" class="ogDesc" value="${dayContent.title}"/>
                <div class="title"><h3>${dateFR}</h3></div>
                <div class="description">
                    <div><strong>Thème du jour :</strong> ${dayContent.title}</div>
                    <div class="places">
                    ${Object.keys(places).map(k => places[k]).join("\n")}
                    </div>
                    <div>
                    ${Object.keys(placesSubPages).map(id => {
						if (placesSubPages[id] === 'done') return '';
						const res = placesSubPages[id];
						placesSubPages[id] = 'done';
						return res;
					}).join("\n")}
                    </div>
                </div>
                <div class="sessions">
	                ${dayTimeline}
	                <div class="sessionList">
                		${allStreamsSessions.join("\n")}
                	</div>
                </div>
            </li>
	`;
}
function address2microData(address){
	const originalAddress = address;
	let streetAddress;
	let postalCode;
	let city;
	originalAddress.replace(/[0-9]{5,7} /,cp=>postalCode=cp.trim());
	const addressParts = originalAddress.split(postalCode);
	city = addressParts[1].trim();
	streetAddress = addressParts[0].trim();
	if(streetAddress[streetAddress.length-1]===',') streetAddress = streetAddress.slice(0,-1);
	return `"streetAddress":"${streetAddress}", "postalCode":"${postalCode}", "addressLocality":"${city}"`
}
function address2json(address){
	const originalAddress = address;
	let streetAddress;
	let postalCode;
	let addressLocality;
	originalAddress.replace(/[0-9]{5,7} /,cp=>postalCode=cp.trim());
	const addressParts = originalAddress.split(postalCode);
	addressLocality = addressParts[1].trim();
	streetAddress = addressParts[0].trim();
	if(streetAddress[streetAddress.length-1]===',') streetAddress = streetAddress.slice(0,-1);
	return {"@type": "PostalAddress",streetAddress,postalCode,addressLocality};
}
function buildPlace(stream, currentStreamNumber, placesSubPages, places) {
	const dayContent = stream.parent;
	const place = parentOrSelf(stream.room);
	if(!place.photo) place.photo = [];
	if(typeof place.photo === 'string') place.photo = [place.photo];
	stream.roomNode = buildHtmlRoomSymbol(currentStreamNumber, dayContent.streams.length, stream.room.title);

	if (placesSubPages[place.uid] !== 'done') placesSubPages[place.uid] = `
<section id="place_${place.uid}" class="subPage">
	<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@graph": [
    {
      "@type": "Place",
      "name": "${place.title}",
      "isAccessibleForFree": true,
      "url": "${place.web}",
      "address": {
        "@type": "PostalAddress",
        ${address2microData(place.address)}
      },
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": ${place.gps.lat},
        "longitude": ${place.gps.lon}
      },
      ${place.photo?'"photos": ['+place.photo.map(urlPhoto =>`{"@type":"ImageObject", "contentUrl":"${urlPhoto}"}`).join(',')+'],':''}
      "description":${JSON.stringify(place.description)}
    }
  ]
}
	</script>
    <h4>${place.title}</h4>
    <address class="desc">${place.address}</address>
    <p>Coordonnées GPS : latitude <span>${place.gps.lat}</span> ; longitude <span>${place.gps.lon}</span></p>
    <p class="maps">Voir sur cartes :
    <a href="https://www.openstreetmap.org/?mlat=${place.gps.lat}&mlon=${place.gps.lon}#map=12/${place.gps.lat}/${place.gps.lon}" rel="noreferrer" target="_blank">OpenStreetMap</a>
    ou
    <a href="https://www.google.com/maps/place/=${place.gps.lat},${place.gps.lon}/" rel="noreferrer" target="_blank">Google Maps</a>
    </p>
    <p><a href="${place.web}" rel="noreferrer" target="_blank">Site web officiel du lieu</a></p>
    <div class="desc">
    	${md(place.description)}
    </div>
    ${place.photo.map(urlPhoto =>`<img src="${urlPhoto}" alt="${place.title}"/>`).join('')}
</section>`;

	const rooms = dayContent.streams.map((stream, currentStreamNumber) => parentOrSelf(stream.room).uid === place.uid?`<li><span class="room">${buildHtmlRoomSymbol(currentStreamNumber + 1, dayContent.streams.length, stream.room.title)}<span>${stream.room.title}</span><span class="roomDetails">${stream.room.description}</span></span></li>`:'').join("\n");
	places[place.uid] = `
	<strong>Lieu :</strong> <a href="#place_${place.uid}" class="subPageLink">${place.title}</a>
	<ul>${rooms}</ul>
`;
}
function time2HHmm(timestamp){
	return dateFormat(new Date(timestamp), 'HH[h]mm', {locale: fr},);
}
function buildDayTimeline(computedStreams) {
	const dayStart = computedStreams.map(stream => stream.map(session => session.start).reduce((acc, curr) => Math.min(acc, curr.getTime()), +Infinity)).reduce((acc, curr) => Math.min(acc, curr), +Infinity);
	const dayEnd = computedStreams.map(stream => stream.map(session => session.end).reduce((acc, curr) => Math.max(acc, curr.getTime()), -Infinity)).reduce((acc, curr) => Math.max(acc, curr), -Infinity);
	const range = dayEnd - dayStart;
	const timelines = computedStreams.map((stream,i) => {
		const timeline = stream.map(session => {
				const start = session.start.getTime() - dayStart;
				const end = session.end.getTime() - dayStart;
				const duration = end - start;
				const topPercent = Math.round(10000*start/range)/100;
				const heightPercent = Math.round(10000*duration/range)/100;
				const speakers = session.who ? (Array.isArray(session.who) ? session.who : [session.who]) : [];
				const speakersClasses = speakers.map(speaker=>`speaker_${speaker.uid}`).join(' ');
			return `<a class="timeBloc ${speakersClasses}" style="top:${topPercent}%;height:${heightPercent}%" href="#session_${session.uid}" title="${time2HHmm(session.start.getTime())} - ${time2HHmm(session.end.getTime())}"></a>`
			});
		return `<div class="timeline room_${i+1} room_${i+1}_in_${computedStreams.length}">${timeline.join('')}</div>`
		}
	);
	// console.log(
	// 	time2HHmm(dayStart),
	// 	time2HHmm(dayEnd),
	// );
	return `<div class="timelines">${timelines.join('')}</div>`;
}
function extractJsonLD(str){
	let ld;
	str.replace(/<script type="application\/ld\+json">(((?!<\/script>)[^])*)<\/script>/,(full, match)=>ld = JSON.parse(match));
	return ld;
}

/**
 *
 * service-worker -> site accessible offline
 *
 * stat conf/ateliers speakers etc
 *
 * Précédent suivant
 *
 * https://secure.meetup.com/meetup_api/key/ et https://www.meetup.com/fr-FR/meetup_api/docs/2/event/?uri=%2Fmeetup_api%2Fdocs%2F2%2Fevent%2F#create
 *
 * partage FB + ics unitaire
 *
 * favoris, enregistrer ses confs + ics sélection
 *
 * vérif horaires salles
 *
 */
function buildProgramme(data){

	const daysHTML = [];
	const placesSubPages = {};

	for (let dayContent of data.days) {
		dayContent.parent = data;
		const places={};
		let allStreamsSessions=[];
		const computedStreams = [];
		let currentStreamNumber = 0;
		for(let stream of dayContent.streams){
			stream.parent = dayContent;
			currentStreamNumber++;
			buildPlace(stream, currentStreamNumber, placesSubPages, places);

			const computedStream = computeStream(stream);
			allStreamsSessions = allStreamsSessions.concat(computedStream.map(cs=>cs.html));
			computedStreams.push(computedStream);
		}
		const dayTimeline = buildDayTimeline(computedStreams);

		daysHTML.push(buildDay(allStreamsSessions, dayContent, dayTimeline, places, placesSubPages));
	}
  const jsonSessions = daysHTML.map(extractJsonLD).map(s=>s['@graph'][0]);
  let jsonSpeakers = jsonSessions.map(s=>s.performer).reduce((acc,cur)=>{
    cur.forEach(c=>acc[c.name]=c);
    return acc;
  },{});
  jsonSpeakers = Object.keys(jsonSpeakers).map(k=>jsonSpeakers[k]);

  const jsonLD = {
	"@context": "http://schema.org",
	"@graph": [
		{
			"@type": "Event",
			"name": conf.title,
			"startDate": conf.date.start,
			"endDate": conf.date.end,
			"isAccessibleForFree": true,
			"inLanguage": "French",
			"image": `${conf.canonical}${conf.og.images_path}${conf.og.default_image}`,
			"description": conf.og.description,
			"location": {
				"@type": "Place",
				"name": conf.og.default_city,
				"isAccessibleForFree": true,
				"address": {
					"@type": "PostalAddress",
					"postalCode": conf.og.default_postal_code,
					"addressLocality": conf.og.default_city
				}
			},
      "offers":[
        {
          "@type": "Offer",
          "url": `${conf.canonical}programme.html`,
          "price": 0,
          "priceCurrency":"Ǧ1",
          "availability": "http://schema.org/InStock",
          "validFrom": dateFormat(addMonths(new Date(conf.date.start),-6),'YYYY-MM-DD')
        }
      ],
      "performer": jsonSpeakers,
      "subEvent": jsonSessions
		}
	]
};
	return `
<script type="application/ld+json">${JSON.stringify(jsonLD)}</script>
<h3>Qui :</h3>
<ul class="speakersList">${data.speakers.sort((a, b) => a.uid.localeCompare(b.uid)).map(
	speaker=>`<li class="speakerHead speaker_${speaker.uid}" style="width: ${Math.floor(10000/data.speakers.length)/100}%"><img class="avatar" src="${speaker.photo?speaker.photo:'img/picto/face.svg'}" alt="${speaker.title}"/><div class="name">${speaker.title.replace(/]/,']<br/>')}</div></li>`).join('')}
</ul>
<h3>Quand :</h3>
<ul>
${daysHTML.join("\n")}
</ul>
`;
}

fs.writeFileSync('generated.programme.html',buildProgramme(data), 'utf8');
