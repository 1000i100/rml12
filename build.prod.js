const now = (new Date).toISOString();
const fs = require("fs");
const yaml = require('js-yaml');
const md = require('marked');
md.setOptions({headerIds: false, xhtml: true});
const templatePages = {};
fs.readdirSync('template/pages/').forEach((f)=>templatePages[f.split('.')[0]] = md(fs.readFileSync('template/pages/'+f, 'utf8')));
fs.readdirSync('template/fragments/').forEach((f)=>templatePages['#'+f.split('.')[0]] = md(fs.readFileSync('template/fragments/'+f, 'utf8')));
templatePages["__programme"] = fs.readFileSync('generated.programme.html', 'utf8');
templatePages["__now"] = now;
function recursiveTemplateFill(templatePages){
  let changed = false;
  for(let key in templatePages){
    let value = templatePages[key];
    if(value.indexOf('{build:')!== -1){
      templatePages[key] = applyData2Template(templatePages,value);
      if (value !== templatePages[key]) changed = true;
      else console.error(`Template inconnu dans ${key}`);
    }
  }
  if(changed) recursiveTemplateFill(templatePages);
}
recursiveTemplateFill(templatePages);
replacesInFiles(["index.html","sitemap.xml", "humans.txt"],templatePages);

function replacesInFiles(fileList,buildReplaces) {
    const configReplaces = yml2replaceMap('event.config.yml');
    for(let fileName of fileList){
      let fileContent = fs.readFileSync(`template/${fileName}`,"utf8");

      fileContent = applyData2Template(buildReplaces,fileContent, 'build');
      fileContent = applyData2Template(configReplaces,fileContent,'conf');
      // anchors to SEO links
      fileContent = fileContent.replace(/="#([^"]*)"/g,(match,catched)=>`="./${catched?catched+'.html':''}"`);

      fs.writeFileSync(`generated.public/${fileName}`,fileContent,"utf8");
    }
}
function yml2replaceMap(dataFile){
  const data = yaml.safeLoad(fs.readFileSync(dataFile, 'utf8'));
  return flatTree(data);
}
function flatTree(obj) {
  if(typeof obj !== 'object') return obj;
  const res = {};
  for(let key in obj){
    const value = obj[key];
    if(typeof value !== 'object') res[key]=value;
    else for (let subKey in value) res[key+'.'+subKey]=flatTree(value[subKey]);
  }
  return res;
}
function applyData2Template(replaceData,templateStr,prefix='build') {
  const regex = new RegExp(Object.keys(replaceData).map(key=>`\\{${prefix}\\:${escapeRegex(key)}\\}`).join('|'),'g');
  const replacements = {};
  for (let k in replaceData) replacements[`{${prefix}:${k}}`]=replaceData[k];

  return templateStr.replace(regex,match=>replacements[match]);
}
function escapeRegex(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};
